import java.util.Date;

public class Cita {

    private String nombreMascota;
    private int dia;
    private int mes;
    private int agnio;
    private int hora;
    private int minutos;
    private String observaciones;

    public String getNombreMascota() {
        return nombreMascota;
    }

    public void setNombreMascota(String nombreMascota) {
        this.nombreMascota = nombreMascota;
    }

    public int getDia() {
        return dia;
    }

    public void setDia(int dia) {
        this.dia = dia;
    }

    public int getMes() {
        return mes;
    }

    public void setMes(int mes) {
        this.mes = mes;
    }

    public int getAgnio() {
        return agnio;
    }

    public void setAgnio(int agnio) {
        this.agnio = agnio;
    }

    public int getHora() {
        return hora;
    }

    public void setHora(int hora) {
        this.hora = hora;
    }

    public int getMinutos() {
        return minutos;
    }

    public void setMinutos(int minutos) {
        this.minutos = minutos;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    @Override
    public String toString(){

        return " a nombre de "+ this.nombreMascota+ " para la fecha "+this.dia+"/"+this.mes+"/"+this.agnio;

    }

    public Cita(String nombreMascota, int dia, int mes, int agnio, int hora, int minutos, String observaciones) {
        this.nombreMascota = nombreMascota;
        this.dia = dia;
        this.mes = mes;
        this.agnio = agnio;
        this.hora = hora;
        this.minutos = minutos;
        this.observaciones = observaciones;

    }

    public Cita() {
    }
}

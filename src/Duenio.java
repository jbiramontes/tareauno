public class Duenio {

    private String nombreDueno;
    private String apellidoUnoDueno;
    private String apellidoDosDueno;
    private int cedulaDueno;
    private String telefono;
    private String direccion;


    public String getNombreDueno() {
        return nombreDueno;
    }

    public void setNombreDueno(String nombreDueno) {
        this.nombreDueno = nombreDueno;
    }

    public String getApellidoUnoDueno() {
        return apellidoUnoDueno;
    }

    public void setApellidoUnoDueno(String apellidoUnoDueno) {
        this.apellidoUnoDueno = apellidoUnoDueno;
    }

    public String getApellidoDosDueno() {
        return apellidoDosDueno;
    }

    public void setApellidoDosDueno(String apellidoDosDueno) {
        this.apellidoDosDueno = apellidoDosDueno;
    }

    public int getCedulaDueno() {
        return cedulaDueno;
    }

    public void setCedulaDueno(int cedulaDueno) {
        this.cedulaDueno = cedulaDueno;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public Duenio(String nombreDueno, String apellidoUnoDueno, String apellidoDosDueno, int cedulaDueno, String telefono, String direccion) {
        this.nombreDueno = nombreDueno;
        this.apellidoUnoDueno = apellidoUnoDueno;
        this.apellidoDosDueno = apellidoDosDueno;
        this.cedulaDueno = cedulaDueno;
        this.telefono = telefono;
        this.direccion = direccion;
    }

    public Duenio() {
    }

    @Override
    public String toString() {
        return " " +
                "" + nombreDueno + " " +
                "" + apellidoUnoDueno + " " +
                "" + apellidoDosDueno + " " +
                ", cedula=" + cedulaDueno +
                ", telefono='" + telefono + '\'' +
                ", direccion='" + direccion + '\'' +
                '}';
    }
}

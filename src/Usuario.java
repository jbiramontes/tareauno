public class Usuario {
    private String nombreUsuario;
    private String apellidoUno;
    private String apellidoDos;
    private String rol;
    private String cedula;
    private String telefono;
    private String direccion;
    private String estado;

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getApellidoUno() {
        return apellidoUno;
    }

    public void setApellidoUno(String apellidoUno) {
        this.apellidoUno = apellidoUno;
    }

    public String getApellidoDos() {
        return apellidoDos;
    }

    public void setApellidoDos(String apellidoDos) {
        this.apellidoDos = apellidoDos;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Override
    public String toString() {

        return "Usuario: [" + this.nombreUsuario + " " + this.apellidoUno + " " + this.apellidoDos + ", " + this.cedula + ", " +
                this.rol + ", " + this.telefono + ", " + this.direccion + ", " + this.estado + "]";

    }

    public Usuario(String nombreUsuario, String apellidoUno, String apellidoDos, String rol, String cedula, String telefono,
                   String direccion, String estado) {
        this.nombreUsuario = nombreUsuario;
        this.apellidoUno = apellidoUno;
        this.apellidoDos = apellidoDos;
        this.rol = rol;
        this.cedula = cedula;
        this.telefono = telefono;
        this.direccion = direccion;
        this.estado = estado;

    }

    public Usuario() {
    }


}

public class Reservacion {

    private String nombreMascota;
    private int diaEntrada;
    private int mesEntrada;
    private int agnioEntrada;
    private int diaSalida;
    private int mesSalida;



    public String getNombreMascota() {
        return nombreMascota;
    }

    public void setNombreMascota(String nombreMascota) {
        this.nombreMascota = nombreMascota;
    }

    public int getDiaEntrada() {
        return diaEntrada;
    }

    public void setDiaEntrada(int diaEntrada) {
        this.diaEntrada = diaEntrada;
    }

    public int getMesEntrada() {
        return mesEntrada;
    }

    public void setMesEntrada(int mesEntrada) {
        this.mesEntrada = mesEntrada;
    }

    public int getAgnioEntrada() {
        return agnioEntrada;
    }

    public void setAgnioEntrada(int agnioEntrada) {
        this.agnioEntrada = agnioEntrada;
    }

    public int getDiaSalida() {
        return diaSalida;
    }

    public void setDiaSalida(int diaSalida) {
        this.diaSalida = diaSalida;
    }

    public int getMesSalida() {
        return mesSalida;
    }

    public void setMesSalida(int mesSalida) {
        this.mesSalida = mesSalida;
    }



    public Reservacion(String nombreMascota, int diaEntrada, int mesEntrada, int agnioEntrada, int diaSalida, int mesSalida) {
        this.nombreMascota = nombreMascota;
        this.diaEntrada = diaEntrada;
        this.mesEntrada = mesEntrada;
        this.agnioEntrada = agnioEntrada;
        this.diaSalida = diaSalida;
        this.mesSalida = mesSalida;

    }

    public Reservacion() {
    }

    @Override
    public String toString() {
        return "Reservaciones [" +
                "Mombre de la Mascota='" + nombreMascota + '\'' +
                ", Fecha de entrada= " + diaEntrada + "/" +
                "" + mesEntrada + "/" +
                "" + agnioEntrada +""+
                ", Fecha de salida= " + diaSalida +"/"+
                "" + mesSalida + "/"+
                +agnioEntrada+" "+

                ']';
    }
}

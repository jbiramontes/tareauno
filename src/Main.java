import java.io.PrintStream;
import java.util.Scanner;

public class Main {
    private static Scanner entrada = new Scanner(System.in);
    private static Cita[] arregloDeCitas = new Cita[1000];
    private static PrintStream salida = new PrintStream(System.out);
    private static int[] identificacion = new int[1000];
    private static Usuario[] arregloDeUsuarios = new Usuario[1000];
    private static Mascota[] arregloMascotas = new Mascota[1000];
    private static Reservacion[] arregloReservaciones = new Reservacion[1000];


    public static void main(String[] args) {
        int opcion;


        do {

            mostrarMenu();
            opcion = entrada.nextInt();
            menu(opcion);

        } while (opcion != 9);

    }//Fin main

    static void mostrarMenu() {

        System.out.println("1. Citas");
        System.out.println("2. Registrar usuario");
        System.out.println("3. Registrar mascota");
        System.out.println("4. Realizar reservación");
        System.out.println("5. Listado de citas");
        System.out.println("6. Listado de usuarios");
        System.out.println("7. Listado de mascotas");
        System.out.println("8. Listado de reservaciones");
        System.out.println("9. Salir");
        System.out.println("Selecciona alguna opción");

    }


    static void menu(int pOpcion) {


        switch (pOpcion) {
            case 1:
                salida.println("Digite el nombre de la mascota: ");
                String nombre = entrada.next();
                salida.println("Digite el dia del mes");
                int dia = entrada.nextInt();
                salida.println("Digite el numero de mes");
                int mes = entrada.nextInt();
                salida.println("Digite el año");
                int agnio = entrada.nextInt();
                salida.println("Digite solamente la hora de la cita utilizando formato militar");
                int hora = entrada.nextInt();
                salida.println("Digite solamente los minutos de la hora de la cita");
                int minutos = entrada.nextInt();
                salida.println("Ingrese aca observaciones acerca de la cita de la mascota");
                entrada.nextLine();
                String observaciones = entrada.nextLine();
                Cita nueva = new Cita(nombre, dia, mes, agnio, hora, minutos, observaciones);
                for (int i = 0; i < arregloDeCitas.length; i++) {
                    if (arregloDeCitas[i] == null) {
                        arregloDeCitas[i] = nueva;
                        i = arregloDeCitas.length;
                    }//fin if
                }//fin for
                break;

            case 2:
                salida.println("Digite la cedula");
                String cedula = entrada.next();
                for (int v = 0; v < arregloDeUsuarios.length; v++) {
                    Usuario temp = arregloDeUsuarios[v];
                    if (arregloDeUsuarios[v] != null) {
                        do {
                            if (temp.getCedula().equals(cedula)) {
                                System.out.println("No se puede registrar usuario ya existe con ese ID");
                                salida.println("Digite la cedula");
                                cedula = entrada.next();
                            }
                        } while (temp.getCedula().equals(cedula));
                    }
                }
                salida.println("Digite el nombre: ");
                String nombreUsuario = entrada.next();
                salida.println("Digite el primer apellido");
                String apellidoUno = entrada.next();
                salida.println("Digite el segundo apellido");
                String apellidoDos = entrada.next();
                salida.println("Rol del usuario");
                String rol = entrada.next();
                salida.println("Telefono del usuario");
                String telefono = entrada.next();
                salida.println("Ingrese la direccion del domicilio");
                entrada.nextLine();
                String domicilio = entrada.nextLine();
                salida.println("Estado del usuario Activo o Inactivo");
                String estado = entrada.next();
                Usuario nuevo = new Usuario(nombreUsuario, apellidoUno, apellidoDos, rol, cedula, telefono, domicilio, estado);
                for (int i = 0; i < arregloDeUsuarios.length; i++) {
                    if (arregloDeUsuarios[i] == null) {
                        arregloDeUsuarios[i] = nuevo;
                        i = arregloDeUsuarios.length;
                    }//fin if
                }//fin for
                break;

            case 3:
                salida.println("Ingrese el nombre de la mascota");
                String nombreMascota = entrada.next();
                for (int v = 0; v < arregloMascotas.length; v++) {
                    Mascota temp = arregloMascotas[v];
                    if (arregloMascotas[v] != null) {
                        do {
                            if (temp.getNombreMascota().equals(nombreMascota)) {
                                System.out.println("No se puede registrar la mascota ese nombre ya existe");
                                salida.println("Ingrese el nomobre de la mascota");
                                nombreMascota = entrada.next();
                            }
                        } while (temp.getNombreMascota().equals(nombreMascota));
                    }
                }
                salida.println("Favor coloque aqui un archivo con la foto de su mascota: ");
                String fotoMascota = entrada.next();
                salida.println("Observaciones de la mascota anotelas a continuacion");
                entrada.nextLine();
                String observacionesMascota = entrada.nextLine();
                salida.println("Seleccione el ranking de la mascota");
                int ranking = entrada.nextInt();
                salida.println("Digite el nombre de dueño: ");
                String nombreDuenio = entrada.next();
                salida.println("Digite el primer apellido");
                String apellidoUnoDuenio = entrada.next();
                salida.println("Digite el segundo apellido");
                String apellidoDosDuenio = entrada.next();
                salida.println("Cedula del Dueño");
                int cedulaDuenio = entrada.nextInt();
                salida.println("Registre un numero de telefono");
                String telefonoDuenio = entrada.next();
                salida.println("Ingrese la direccion");
                entrada.nextLine();
                String direccion = entrada.nextLine();
                Duenio duenio = new Duenio(nombreDuenio, apellidoUnoDuenio, apellidoDosDuenio, cedulaDuenio, telefonoDuenio, direccion);
                Mascota nuevaMascota = new Mascota(nombreMascota, fotoMascota, observacionesMascota, ranking, duenio);
                for (int i = 0; i < arregloMascotas.length; i++) {
                    if (arregloMascotas[i] == null) {
                        arregloMascotas[i] = nuevaMascota;
                        i = arregloMascotas.length;
                    }//fin if
                }//fin for
                break;

            case 4:
                salida.println("Digite el nombre de la mascota: ");
                String nombreReservacion = entrada.next();
                System.out.println("--Fecha de entrada--");
                salida.println("Digite el dia");
                int diaEntrada = entrada.nextInt();
                salida.println("Digite el numero del mes");
                int mesEntrada = entrada.nextInt();
                salida.println("Digite el año");
                int agnioReservacion = entrada.nextInt();
                System.out.println("--Fecha de salida--");
                salida.println("Digite el dia");
                int diaSalida = entrada.nextInt();
                salida.println("Digite el mes");
                int mesSalida = entrada.nextInt();

                Reservacion nuevaReservacion = new Reservacion(nombreReservacion, diaEntrada, mesEntrada, agnioReservacion, diaSalida, mesSalida);
                for (int i = 0; i < arregloReservaciones.length; i++) {
                    if (arregloReservaciones[i] == null) {
                        arregloReservaciones[i] = nuevaReservacion;
                        i = arregloDeCitas.length;
                    }//fin if
                }//fin for
                System.out.println("**Su reservacion ha sido registrada**");

                break;


            case 5:
                for (int i = 0; i < arregloDeCitas.length; i++) {
                    if (arregloDeCitas[i] != null) {
                        salida.println("-Cita" + (i + 1) + " " + arregloDeCitas[i]);
                    }//fin if
                }//fin for

                break;
            case 6:
                for (int i = 0; i < arregloDeUsuarios.length; i++) {
                    if (arregloDeUsuarios[i] != null) {
                        salida.println("- " + (i + 1) + " " + arregloDeUsuarios[i]);
                    }//fin if
                }//fin for

                break;

            case 7:

                for (int i = 0; i < arregloMascotas.length; i++) {
                    if (arregloMascotas[i] != null) {
                        salida.println("- " + (i + 1) + " " + arregloMascotas[i]);
                    }//fin if
                }//fin for


                break;
            case 8:
                for (int i = 0; i < arregloReservaciones.length; i++) {
                    if (arregloReservaciones[i] != null) {
                        salida.println("- " + (i + 1) + " " + arregloReservaciones[i]);
                    }//fin if
                }//fin for



                break;

            default:
                System.out.println("Selecciona alguna opcion");
                break;
        }


    }//fin menu

}//Fin programa

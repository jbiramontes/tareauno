public class Mascota {
    private String nombreMascota;
    private String fotoMascota;
    private String observaciones;
    private int ranking;
    private Duenio duenio;


    public String getNombreMascota() {
        return nombreMascota;
    }

    public void setNombreMascota(String nombreMascota) {
        this.nombreMascota = nombreMascota;
    }

    public String getFotoMascota() {
        return fotoMascota;
    }

    public void setFotoMascota(String fotoMascota) {
        this.fotoMascota = fotoMascota;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public int getRanking() {
        return ranking;
    }

    public void setRanking(int ranking) {
        this.ranking = ranking;
    }

    public Duenio getDuenio() {
        return duenio;
    }

    public void setDuenio(Duenio duenio) {
        this.duenio = duenio;
    }
    public Mascota(String nombreMascota, String fotoMascota, String observaciones, int ranking, Duenio duenio) {
        this.nombreMascota = nombreMascota;
        this.fotoMascota = fotoMascota;
        this.observaciones = observaciones;
        this.ranking = ranking;
        this.duenio= duenio;
    }

    public Mascota() {
    }

    @Override
    public String toString() {
        return "Mascota [" +
                "Nombre='" + nombreMascota + '\'' +
                ", foto='" + fotoMascota + '\'' +
                ", observaciones='" + observaciones + '\'' +
                ", ranking=" + ranking +
                ", propietario="+duenio+
                '}';
    }


}
